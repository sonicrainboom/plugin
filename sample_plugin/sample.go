package main

import (
	"fmt"
	"sonicrainboom.rocks/plugin/sdk"
	"sonicrainboom.rocks/plugin/shared"
	"time"
)

type Sample struct {
	sdk.BasicPlugin
	config *shared.PluginConfig
}

func (p *Sample) Init(config *shared.PluginConfig) (err error) {
	p.config = config
	return
}

func (p *Sample) Main() (err error) {
	for {
		fmt.Printf("strings %#v\n", p.config.Strings)
		fmt.Printf("ints %#v\n", p.config.Ints)
		fmt.Printf("bools %#v\n", p.config.Bools)

		time.Sleep(time.Second)
	}

	return
}

func main() {
	sdk.RunPlugin(
		&shared.PluginInfo{
			Name:      "Sample Plugin",
			Version:   "0.0.0",
			Developer: "Hendrik 'T4cC0re' Meyer",
			Type:      0,
		},
		&Sample{},
	)
}
