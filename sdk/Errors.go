package sdk

import "sonicrainboom.rocks/srberrors"

var ErrNotImplemented = srberrors.New("not implemented", srberrors.ETypePlugin)
var ErrInitFailed = srberrors.New("init failed", srberrors.ETypePlugin)
