package sdk

import (
	"context"
	"log"
	"sonicrainboom.rocks/srberrors"
	"sonicrainboom.rocks/plugin/shared"
	"sync"
	"time"
)

var launchLock = sync.Mutex{}
var shouldKeepRunning = true

/**
SRBPlugin outlines the minimum surface of a plugin.
*/
type SRBPlugin interface {
	Main() error
	Init(*shared.PluginConfig) error
}

/**
pluginSDKShim is used internally to implement the required gRPC calls in a
binary using this SDK.
*/
type pluginSDKShim struct {
	shared.UnimplementedCorePluginServer
	plugin SRBPlugin
	info   *shared.PluginInfo
}

/**
Plugin_Identify is used internally by the plugin system.

This simply returns the info passed to the struct.
*/
func (shim *pluginSDKShim) Plugin_Identify(context.Context, *shared.PluginInfoQuery) (*shared.PluginInfo, error) {
	log.Printf("PluginSDK: Plugin_Identify called\n")
	return shim.info, nil
}

/**
Plugin_Initialize is used internally by the plugin system.

This is the callback from core, after the plugin was registered via the identity in the response of it's call to Plugin_Identify.
This method's responsibility is to take the configuration, and pass it to the Plugin's Init method.

To signalize to RunPlugin, that the initialization has finished/failed, this method must release the launchLock in all cases.
*/
func (shim *pluginSDKShim) Plugin_Initialize(_ context.Context, config *shared.PluginConfig) (ack *shared.PluginConfigAcknowledgement, err error) {
	log.Printf("PluginSDK: Plugin_Initialize called\n")
	ack = &shared.PluginConfigAcknowledgement{}
	err = srberrors.Wrap(shim.plugin.Init(config))
	log.Printf("PluginSDK: plugin.Init() finished\n")
	if srberrors.Is(err, nil, ErrNotImplemented) {
		// When Init() is not overridden and we have no other errors:
		err = nil
		shouldKeepRunning = true
	} else {
		shouldKeepRunning = false
	}
	log.Printf("PluginSDK: releasing launchLock\n")
	launchLock.Unlock()
	log.Printf("PluginSDK: launchLock released\n")

	return
}

/**
RunPlugin will connect a SRBPlugin to SonicRainBoom core.

It will perform the announcement and handle the initialization.
Once the passed SRBPlugin is initialized, this function will block until
the plugin's Main has returned.
*/
func RunPlugin(info *shared.PluginInfo, plugin SRBPlugin) {
	log.Printf("PluginSDK: RunPlugin called\n")
	shim := &pluginSDKShim{
		plugin: plugin,
		info:   info,
	}
	port, err := listen(context.Background(), shim)
	err = srberrors.Wrap(err)
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("PluginSDK: aquiring launchLock...\n")

	launchLock.Lock()

	log.Printf("PluginSDK: Announcing to core...\n")

	// The announce will lead to Plugin_Identify and Plugin_Initialize being called by core.
	err = srberrors.Wrap(announce(context.Background(), port))
	if err != nil {
		log.Fatalln(err)
	}

	// This lock will be released by Plugin_Initialize
	launchLock.Lock()
	log.Printf("PluginSDK: aquired launchLock after Init()\n")
	if !shouldKeepRunning {
		log.Fatalln(srberrors.Wrap(ErrInitFailed))
	}

	go func() {
		for {
			time.Sleep(time.Second)
			_, err := CoreClient().Ping(context.Background(),&shared.Acknowledgement{})
			err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork)
			if err != nil {
				log.Fatalln(err)
			}
		}
	}()

	log.Printf("PluginSDK: running Main()\n")
	err = srberrors.Wrap(plugin.Main())
	if err != nil {
		log.Fatalln(err)
	}
}
