package sdk

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	"net"
	"sonicrainboom.rocks/plugin/shared"
	"sonicrainboom.rocks/srberrors"
	"time"
)

var coreClient shared.SRBCoreClient

/**
CoreClient returns a gRPC client to SonicRainBoom core.
This is only available *after* the plugin was announced to core.

This client is usable in the plugin's Init() and Main() methods.
*/
func CoreClient() shared.SRBCoreClient {
	return coreClient
}

func announce(ctx context.Context, port uint16) (err error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	log.Printf("Connecting to SRB Core...\n")
	var conn *grpc.ClientConn
	ctx, _ = context.WithTimeout(ctx, time.Second)
	conn, err = grpc.DialContext(ctx, fmt.Sprintf("127.0.0.1:%d", srbPort), opts...)
	err = srberrors.WrapInType(err, srberrors.ETypeUnflaggedExternal)
	log.Printf("%#v\n", err)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Connected to SRB Core\n")
	}
	client := shared.NewSRBCoreClient(conn)
	var ack *shared.Acknowledgement
	ack, err = client.Announce(ctx, &shared.Announcement{
		Cookie: cookie,
		Port:   uint32(port),
	})
	err = srberrors.WrapInType(err, srberrors.ETypeUnflaggedExternal)
	if err != nil {
		log.Fatal(err)
	} else {
		log.Print("Announced plugin to SRB Core\n")
	}

	_ = ack

	coreClient = client

	return
}

func listen(ctx context.Context, impl shared.CorePluginServer) (port uint16, err error) {
	var lis net.Listener
	port = shared.PORT_RANGE_START
	for port < shared.PORT_RANGE_END {
		lis, err = net.Listen("tcp", fmt.Sprintf("127.0.0.1:%d", port))
		err = srberrors.WrapInType(err, srberrors.ETypePlugin|srberrors.ETypeNetwork|srberrors.ETypeListen)
		if err == nil {
			break
		}
		log.Println(err)
		port++
	}
	if err != nil {
		return 0, err
	}

	log.Printf("Plugin Listening on %s...", lis.Addr().String())
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	shared.RegisterCorePluginServer(grpcServer, impl)
	go grpcServer.Serve(lis)
	log.Printf("Plugin Serving...")
	return
}
