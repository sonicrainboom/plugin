package sdk

import (
	"flag"
	"log"
	"os"
)

var srbPort uint16
var cookie string

func init() {
	flag.StringVar(&cookie, "plugin.cookie", cookie, "SonicRainBoom plugin cookie")
	p := flag.Uint("core.port", 0, "SonicRainBoom core gRPC port")
	flag.Parse()

	if *p > 65525 {
		panic("core.port too large")
	}
	srbPort = uint16(*p)
	if cookie == "" {
		panic("plugin.cookie not passed")
	}

	log.Printf("Starting Plugin with cookie '%s' and SRB on port %d\n", cookie, srbPort)

	argv0 := os.Args[0]
	os.Args = append([]string{}, argv0)
	os.Args = append(os.Args, flag.Args()...)
}
