package sdk

import "sonicrainboom.rocks/plugin/shared"

/**
BasicPlugin is an embeddable, minimal plugin.
*/
type BasicPlugin struct {
}

/**
Main will be called once the plugin is initialized by Init successfully.
This is the plugin's main entrypoint and runs non-blocking from the core.

Any plugin MUST implement this
*/
func (plugin *BasicPlugin) Main() (err error) {
	return ErrNotImplemented
}

/**
Init will be called once the plugin is registered with SonicRainBoom.
This can be used to initialize anything in the plugin that is required to run
it.
Before this is called, the pointer to the Core gRPC may not be defined.

This method WILL BLOCK SonicRainBoom Core's plugin startup, so ensure this
function returns within some reasonable time.

You MUST NOT store the pointer to the PluginConfig. If you require values from
it, you MUST *copy* them.

Any plugin SHOULD implement this, if it requires initialization.
*/
func (plugin *BasicPlugin) Init(_ *shared.PluginConfig) (err error) {
	return ErrNotImplemented
}
