//go:generate protoc -I ../proto --go_out .. --go-grpc_out .. ../proto/info.proto
//go:generate protoc -I ../proto --go_out .. --go-grpc_out .. ../proto/srb_core.proto

package shared

const PORT_RANGE_START = 49772 // Dynamic ports 49152 - 65535; 49SRB in T9
const PORT_RANGE_END = 50772
