module sonicrainboom.rocks/plugin

go 1.16

require (
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
	sonicrainboom.rocks/srberrors v0.0.0-20210715011724-21e559bff791
)
